package figure

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func TestFigurator_Please(t *testing.T) {
	type Config struct {
		SomeField     int `fig:"foo"`
		AnotherField  int `fig:"-"`
		RequiredField int `fig:"bar,required"`
	}

	cases := []struct {
		name       string
		configData map[string]interface{}
		expected   Config
		err        error
	}{
		{
			name: "check field with tag ignore is not set",
			configData: map[string]interface{}{
				"foo":         123,
				"another_var": 321,
				"bar":         666,
			},
			expected: Config{SomeField: 123, AnotherField: 0, RequiredField: 666},
			err:      nil,
		},
		{
			name: "check err if not enough data for tag required",
			configData: map[string]interface{}{
				"foo":         123,
				"another_var": 321,
			},
			expected: Config{},
			err:      ErrRequiredValue,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			config := Config{}
			err := Out(&config).From(c.configData).Please()
			if c.err == nil {
				assert.EqualValues(t, c.expected, config)
			}

			assert.Equal(t, c.err, errors.Cause(err))
		})
	}
}

func TestInt64Slice(t *testing.T) {
	type TestStruct struct {
		SomeSlice []int64 `fig:"some_slice"`
	}

	testValue := []int64{17, 42}
	testData := TestStruct{}

	err := Out(&testData).From(map[string]interface{}{"some_slice": testValue}).Please()
	assert.NoError(t, err)
	assert.EqualValues(t, testValue, testData.SomeSlice)
}

func TestSliceConfigSource(t *testing.T) {
	var someSlice []int64
	testValue := []int64{17, 42}

	err := Out(&someSlice).FromInterface(testValue).Please()
	assert.NoError(t, err)
	assert.EqualValues(t, testValue, someSlice)
}

func TestPrimitiveConfigSource(t *testing.T) {
	var someInt int64
	testValue := int64(42)

	err := Out(&someInt).FromInterface(testValue).Please()
	assert.NoError(t, err)
	assert.EqualValues(t, testValue, someInt)
}
